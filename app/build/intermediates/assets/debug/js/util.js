$(document).ready(function(e) {
	if(cekSupport()){
		initApp();
	}
});

$(window).load(function(e) {
    $("#loading, #ld").hide();
});

function clickSound() {
	$("a, button, input").click(function (e) {
		playSound();
	});
}

var i=0;
function cekHash() {
	var p = getUrlVars()[0];
	if(p=='navbar'){
		$("#menu").hide();
		$("#back").show();
		if (!$('body').hasClass('overlay-open')) {
			window.history.go(-1);
		}else{
			if(i==0){
			$(".overlay-open .overlay").click(function(e) {
				window.history.go(-1);
			});
			i++;
			}
			$("#menu").show();
			$("#back").hide();
		}
		console.log(i);
	}else{
		if ($('body').hasClass('overlay-open')) {
			$('.overlay').hide();
			$('body').removeClass('overlay-open');
		}
		loadPage();
	}
}

function loadPage() {
	$('.page').hide();
	var p = getUrlVars()[0];
	var uid = window.localStorage.getItem('ID');
	var fr = window.localStorage.getItem('fr');
	$("li[class='active']").removeAttr('class');
	if (typeof (p) == 'undefined' || p == 'http:' || p == 'file:' || p == 'home') {
		$("a[href='#home']").closest('li').addClass('active');
		$('div[data-page="index"]').show();
		initRoute('home');
		$(".navbar-brand").text('mKasir').css('text-transform','none');
		$("#menu, #leftsidebar").show();
		$("#back").hide();
	} else {
		var lht = getUrlVars()['lihat'];
		var edt = getUrlVars()['edit'];
		var hps = getUrlVars()['hapus'];
		var add = getUrlVars()['add'];
		var idd = getUrlVars()['baca'];
		$("a[href='#" + p + "']").closest('li').addClass('active');
		$('div[data-page="'+p+'"]').show();
		initRoute(p);
		var ttl = p.split('-').join(' ');
		$(".navbar-brand").text(ttl).css('text-transform','capitalize');
		$("#menu, #leftsidebar").hide();
		$("#back").show();
	}
}

function getUrlVars() {
	var vars = [],
		hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('/');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}

function notif(pesan) {
	$.notify({
		message: pesan
	}, {
		allow_dismiss: true,
		newest_on_top: true,
		timer: 1500,
		delay: 1500,
		placement: {
			from: 'top',
			align: 'right'
		},
		animate: {
			enter: 'animated bounceInRight',
			exit: 'animated bounceOutRight'
		}
	});
}

function initComponent() {
	$('.back').click(function (e) {
		window.history.go(-1);
	});
	$.AdminBSB.input.activate();
	$.AdminBSB.DatePicker.activate();
	$.AdminBSB.select.activate();
	FastClick.attach(document.body);
	noAutoComplete();
	clickSound();
}

function noAutoComplete() {
	$('input').attr('autocomplete', 'off');
}

function getNotif() {
	$.ajax({
		url: ajaxUrl + '?post=1',
		data: "p=getnotif",
		type: "POST",
		cache: 'false',
		success: function (e) {
			if (e) {
				console.log(e);
				var h = $.parseJSON(e);
				$.ajax({
					url: ajaxUrl + '?post=1',
					type: 'POST',
					data: 'p=rnotif&id=' + h.ID
				});
				createNotification(h.judul, h.isi);
			}
		}
	});
}

function playSound() {
	try{  
		Android.playSound();	
	}catch(error){
		//console.log(error);
	}
}

function createNotification(a, b) {
	android.createNotification(a,b);
}

function getFr() {
	var client = new ClientJS();
	var fingerprint = client.getFingerprint();
	window.localStorage.setItem('fr', fingerprint);
}

function animDiv(elm) {
	var animationName = 'tada';
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	$(elm).addClass('animated ' + animationName).one(animationEnd, function () {
		$(elm).removeClass('animated ' + animationName);
	});
}

function autoHeight(){
	var h = window.innerHeight;
	h=h-220;
	$("#l_cartd").css('height',(h-100));
	$("#lpproduk").css('height',h);
	$("#lproduk, #ltrx").css('height',(h+45));
	$("#lproduk, #lpproduk, #ltrx, #l_cartd").css('overflow-y','scroll').css('overflow-x','hidden');
}
		
