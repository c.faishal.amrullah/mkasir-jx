var sUrl="http://matakata.net/mkasir/client_ajax.php";
var userLoggedIn = window.localStorage.getItem('userLoggedIn');

function initRoute(page){
	switch(page){
		case 'home':
			var phome = $(document).find('div[data-page="index"]:visible').length;
			if(phome){
				initHome();					
			}
		break;
		case 'penjualan':
			var penjualan = $(document).find('div[data-page="penjualan"]:visible').length;
			if(penjualan){
					try{
							cekCart();
						}catch(e){
							//console.log(e);
						}
					var data = JSON.parse(window.localStorage.getItem('mKasir_prd'));
					if(data){
						var produk = $('#pproduk').html();
						var compiledTemplate = Template7.compile(produk);
						var context = data;
						var html = compiledTemplate(context);
						$("#lpproduk").html(html);
					}else{
						swal({
							title: "Belum ada Produk",
							text: 'Silahkan lakukan Sync Produk untuk melakukan Penjualan',
							showCancelButton: false,
							confirmButtonColor: "#2196f3",
							closeOnConfirm: true
						}, function (isConfirm) {
							window.history.go(-1);
						});
					}
			}
		break;
		case 'produk':
			var produk = $(document).find('div[data-page="produk"]:visible').length;
			if(produk){
			var data = JSON.parse(window.localStorage.getItem('mKasir_prd'));
			if(data){
				var produk = $('#produk').html();
				var compiledTemplate = Template7.compile(produk);
				var context = data;
				var html = compiledTemplate(context);
				$("#lproduk").html(html);
				$("#no-prd").hide();
			}else{
				swal({
					title: "Belum ada Produk",
					text: 'Silahkan lakukan Sync Produk untuk Melihat data Produk',
					showCancelButton: false,
					confirmButtonColor: "#2196f3",
					closeOnConfirm: true
				}, function (isConfirm) {
					window.history.go(-1);
				});
			}
		}
		break;
		case 'produk-detail':
			var produk_detail = $(document).find('div[data-page="produk-detail"]:visible').length;
			if(produk_detail){
			var id = getUrlVars()['id'];
			if(id){
					var data = JSON.parse(getPrdDetail(id));
					var produk = $('#detproduk').html();
					var compiledTemplate = Template7.compile(produk);
					var context = data;
					var html = compiledTemplate(context);
					$("#det-prd").html(html);
			}
		}
		break;
		case 'transaksi':
			var transaksi = $(document).find('div[data-page="transaksi"]:visible').length;
			if(transaksi){
			var tr = window.localStorage.getItem('mKasir_trx');
			if(tr){
				var jd = '{"trx":['+tr+']}';
				var data = JSON.parse(jd);
				var produk = $('#trxl').html();
				var compiledTemplate = Template7.compile(produk);
				var context = data;
				var html = compiledTemplate(context);
				$("#ltrx").html(html);
			}else{
				swal({
					title: "Belum ada Transaksi",
					text: 'Silahkan lakukan Penjualan atau Sync Transaksi terlebih dahulu untuk Melihat data Transaksi',
					showCancelButton: false,
					confirmButtonColor: "#2196f3",
					closeOnConfirm: true
				}, function (isConfirm) {
					window.history.go(-1);
				});
			}
		}
		break;
		case 'transaksi-detail':
			var transaksi_detail = $(document).find('div[data-page="transaksi-detail"]:visible').length;
			if(transaksi_detail){
			var id = getUrlVars()['id'];
			if(id){
					var data = getTrxDetail(id);
					var trx = JSON.parse('{"trx":'+(data)+'}');
					var produk = $('#trx_subl').html();
					var compiledTemplate = Template7.compile(produk);
					var context = trx;
					var html = compiledTemplate(context);
					$("#tbsubl").html(html);
					var idl = trx.trx[0].ID;
					var waktul = trx.trx[0].waktu;
					$("#id_trxl").text(idl);
					$("#tgl_trxl").text(waktul);
					var crt = trx.trx[0].data.toString().split("'").join('"');
					var lcart = JSON.parse(crt);
					var produk = $('#cartDatal').html();
					var compiledTemplate = Template7.compile(produk);
					var context = lcart;
					var html = compiledTemplate(context);
					$("#l_cartl").html(html);
			}
		}
		break;
		case 'bayar':
			var bayar = $(document).find('div[data-page="bayar"]:visible').length;
			if(bayar){
			$('.modal').modal('hide');
			var cdata = window.localStorage.getItem('mKasir_cartData');
			if(cdata!=null){
				var dCart = JSON.parse('['+cdata.split('|').join(',')+']');
				if(dCart.length<1){
					$("#btotal").text('0,0');
				}else{
					var rcdata = cdata.split('|');
					var cc = '{"cart":[';
					for(var i=0;i<rcdata.length;i++){
						cc+=rcdata[i]+',';
					}
					cc=cc.substr(0,(cc.length)-1);
					cc+=']}';
					var cD = JSON.parse(cc);
					dCart.sort();
					const count = dCart =>
					  dCart.reduce((a, b) =>
						Object.assign(a, {[b.nama+'|'+b.harga+'|'+b.ID]: (a[b.nama+'|'+b.harga+'|'+b.ID] || 0) + 1}), {});
					var cj = JSON.stringify(count(dCart));
					var a = cj.replace('{','').replace('}','').split(':').join('|').split('"').join('').split(',');
					var ss='{"bcart":[';
					for(var s=0;s<a.length;s++){
						var o = a[s].split('|');
						ss+='{"ID":"'+o[2]+'","nama":"'+o[0]+'","harga":"'+numberWithCommas(o[1])+'","qty":"'+o[3]+'","jml":"'+numberWithCommas(o[1]*o[3])+'"},';
					}
					ss=ss.substr(0,(ss.length)-1);
					ss+=']}';
					var sD = JSON.parse(ss);
					var produk = $('#cartDatab').html();
					var compiledTemplate = Template7.compile(produk);
					var context = sD;
					var html = compiledTemplate(context);
					$("#l_cartb").html(html);
					var total=0;
					for(var h=0;h<cD.cart.length;h++){
						total+=Number(cD.cart[h].harga);
					}
					$("#btotal").text(numberWithCommas(Math.ceil(total)));
					$("#gtotal").text(numberWithCommas(Math.ceil(total)));
					$("#rharga").val(total);
					$("#upas").attr('data-bayar',total);
					var id = getRndInteger(1,999999);
					var waktu = Now();
					$("#id_trx").text(id);
					$("#tgl_trx").text(waktu);
					$('.diskon').on('click',function(){
						var dsk = Number($(this).data('diskon'));
						var htd = (dsk/100)*total;
						var hdiskon = total-htd;
						$("#rharga").val(Math.ceil(hdiskon));
						$("#upas").attr('data-bayar',hdiskon);
						$("#gtotal").text(numberWithCommas(Math.ceil(hdiskon)));
						$("#bdiskon").text(dsk);
						$('#jby').text('0,0');
						$('#kmb').text('0,0');
					});
					$('.diskonlain').on('click', function () {
						swal({
							title: "Berikan Diskon",
							text: "Jumlah Diskon",
							type: "input",
							showCancelButton: true,
							closeOnConfirm: false,
							animation: "slide-from-top",
							inputPlaceholder: "Masukkan Jumlah Diskon"
						}, function (inputValue) {
							if (inputValue === false) return false;
							if (inputValue === "" || isNaN(inputValue) || Number(inputValue)<1) {
								swal.showInputError("Diskon harus berupa angka dan lebih dari Nol!"); return false
							}
							var dsk = Number(inputValue);
							var htd = (dsk/100)*total;
							var hdiskon = total-htd;
							$("#gtotal").text(numberWithCommas(Math.ceil(hdiskon)));
							$("#bdiskon").text(dsk);
							$("#rharga").val(Math.ceil(hdiskon));
							$("#upas").attr('data-bayar',hdiskon);
							swal.close();
						});
					});
					$('.jbayar').on('click',function(){
						$('#jby').text(numberWithCommas(Math.ceil($(this).attr('data-bayar'))));
						$('#kmb').text('0,0');
					});
					$('.jbayarlain').on('click', function () {
						swal({
							title: "Jumlah bayar?",
							text: "Masukkan Jumlah Bayar",
							type: "input",
							showCancelButton: true,
							closeOnConfirm: false,
							animation: "slide-from-top",
							inputPlaceholder: "Masukkan Jumlah Bayar"
						}, function (inputValue) {
							if (inputValue === false) return false;
							if (inputValue === "" || isNaN(inputValue) || Number(inputValue)<1) {
								swal.showInputError("Jumlah bayar harus berupa angka dan lebih dari Nol!"); return false
							}
							var jb = Number(inputValue-$("#rharga").val());
							if(jb>0){
								$('#jby').text(numberWithCommas(Math.ceil(inputValue)));
								$('#kmb').text(numberWithCommas(Math.ceil(jb)));
								swal.close();
							}else{
								swal("Error!", "Jumlah bayar kurang! ", "error");
								$('#jby').text('0,0');
								$('#kmb').text('0,0');
							}
						});
					});
					$('.konbayar').on('click', function (){
						if($("#jby").text()=='0,0'){
							swal({
							title: "Bayar!",
							text: "Masukkan Bayar Tunai!",
							type: "warning",
							closeOnConfirm: true
							}, function(){
								$('#upas').focus();
								animDiv('#upas');
								animDiv('.jbayarlain');
							});
						}else{
						  var phtml = $('#shtb').html();
						  var items_name = [];
						  var items_qty = [];
						  var items_jml = [];
						  swal({
							title: "Konfirmasi?",
							text: "Selesaikan Transaksi?",
							type: "info",
							showCancelButton: true,
							confirmButtonColor: "#2196f3",
							confirmButtonText: "Selesaikan!",
							closeOnConfirm: false
						  }, function () {
							  	var qty;
								var stok_produk;
								var nstok;
								for(var m=0;m<sD.bcart.length;m++){
									qty = Number(sD.bcart[m].qty);
									stok_produk = Number(JSON.parse(getPrdDetail(sD.bcart[m].ID)).produk[0].stok_produk);
									nstok = stok_produk-qty;
									updatePrd(sD.bcart[m].ID,'stok_produk',nstok);
									items_name.push(sD.bcart[m].nama);
									items_qty.push(sD.bcart[m].qty);
									items_jml.push(sD.bcart[m].jml);
								}
								upTrx(ss,id,waktu,$('#btotal').text(),$('#jby').text(),$('#kmb').text(),$("#bdiskon").text(),$('#rharga').val(),'Sukses');
								window.localStorage.removeItem('mKasir_cartData');
								window.history.go(-2);
							swal({
								title: "Transaksi Selesai",
								text: phtml,
								html: true,
								showCancelButton: true,
								confirmButtonColor: "#2196f3",
								confirmButtonText: "Print",
								cancelButtonText: "Tutup",
								closeOnConfirm: true,
								closeOnCancel: true
							},function (isConfirm){
									if (isConfirm) {
										PrintElem("phtml", items_name, items_qty, items_jml);
										//document.location.reload();
									} else {
										//xdocument.location.reload();
									}
								});
						  });
						}
					});
					$('.konbatal').on('click', function (){
						swal({
							title: "Batalkan Transaksi?",
							text: "Anda yakin akan membatalkan Transaksi?",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Batalkan!",
							closeOnConfirm: false
						}, function () {
							window.localStorage.removeItem('mKasir_cartData');
							window.history.go(-2);
							swal({
							title: "Transaksi Dibatalkan",
							text: "Apakah anda ingin menyimpan Transaksi Batal?",
							type: "success",
							showCancelButton: true,
							confirmButtonColor: "#2196f3",
							confirmButtonText: "Simpan",
							cancelButtonText: "Tidak",
							closeOnConfirm: true,
							closeOnCancel: true
							}, function(isConfirm){
								if(isConfirm){
									upTrx(ss,id,waktu,$('#btotal').text(),$('#jby').text(),$('#kmb').text(),$("#bdiskon").text(),$('#rharga').val(),'Batal');
								}
								document.location.reload();
							});
						});
					});
				}
			}
		}
		break;
		case 'settings':
			var settings = $(document).find('div[data-page="settings"]:visible').length;
			if(settings){
				$('[data-settings="tema"]').change(function(e) {
					var theme = $(this).val();
					window.localStorage.setItem('mKasir_appTheme',theme);
					cekTheme();
					try{
						android.changeTheme(theme);
					}catch(e){

					}
				});
				$('[data-settings="nama-usaha"]').on('click', function () {
					swal({
						title: "Nama Usaha?",
						text: "Masukkan Nama Usaha",
						type: "input",
						showCancelButton: true,
						closeOnConfirm: false,
						animation: "slide-from-top",
						inputPlaceholder: "Masukkan Nama Usaha"
					}, function (inputValue) {
						if (inputValue === false) return false;
						if (inputValue === "") {
							swal.showInputError("Masukkan Nama Usaha"); return false
						}
						window.localStorage.setItem('mKasir_NamaUsaha',inputValue);
						cekNamaUsaha();
						swal.close();
					});
				});
				$('[data-settings="auto-sync-produk"]').click(function(e) {
					var asp = $(this).is(':checked');
					if(asp){						
						swal({
							title: "Setiap berapa menit?",
							text: "Masukkan interval",
							type: "input",
							showCancelButton: true,
							closeOnConfirm: false,
							animation: "slide-from-top",
							inputPlaceholder: "Masukkan interval (min : 1, max: 60)"
						}, function (inputValue) {
							if (inputValue === false){
								$('[data-settings="auto-sync-produk"]').removeAttr('checked');
								return false;
							}
							if (inputValue === "" || isNaN(inputValue) || Number(inputValue)<1 || Number(inputValue)>60) {
								swal.showInputError("interval harus berupa angka (min: 1, max: 60)!");
								return false;
							}
							var inv = (Number(inputValue)*60000);
							window.localStorage.setItem('mKasir_autoSyncPrd',inv);
							autoSync();
							swal({
								title: "Auto Sync Produk dijalankan",
								text: "Fitur Auto Sync berjalan setiap "+inputValue+" menit",
								type: "success",
								showCancelButton: false,
								confirmButtonText: "Ok",
								closeOnConfirm: true
							}, function () {
								document.location.reload();
							});
						});
					}else{
						window.localStorage.removeItem('mKasir_autoSyncPrd');
						swal({
						title: "Auto Sync Transaksi dihentikan",
						text: "Fitur Auto Sync berhenti",
						type: "success",
						showCancelButton: false,
						confirmButtonText: "Ok",
						closeOnConfirm: true
						}, function () {
							document.location.reload();
						});
					}
				});

				$('[data-settings="auto-sync-transaksi"]').click(function(e) {
					var asp = $(this).is(':checked');
					if(asp){
						swal({
							title: "Setiap berapa menit?",
							text: "Masukkan interval",
							type: "input",
							showCancelButton: true,
							closeOnConfirm: false,
							animation: "slide-from-top",
							inputPlaceholder: "Masukkan interval (min : 1, max: 60)"
						}, function (inputValue) {
							if (inputValue === false){
								$('[data-settings="auto-sync-transaksi"]').removeAttr('checked');
								return false;
							}
							if (inputValue === "" || isNaN(inputValue) || Number(inputValue)<1 || Number(inputValue)>60) {
								swal.showInputError("interval harus berupa angka (min: 1, max: 60)!");
								return false;
							}
							var inv = (Number(inputValue)*60000);
							window.localStorage.setItem('mKasir_autoSyncTrx',inv);
							autoSync();
							swal({
								title: "Auto Sync Transaksi dijalankan",
								text: "Fitur Auto Sync berjalan setiap "+inputValue+" menit",
								type: "success",
								showCancelButton: false,
								confirmButtonText: "Ok",
								closeOnConfirm: true
							}, function () {
								document.location.reload();
							});
						});
					}else{
						window.localStorage.removeItem('mKasir_autoSyncTrx');
						swal({
							title: "Auto Sync Transaksi dihentikan",
							text: "Fitur Auto Sync berhenti",
							type: "success",
							showCancelButton: false,
							confirmButtonText: "Ok",
							closeOnConfirm: true
						}, function () {
							document.location.reload();
						});
					}
				});
			}
		break;
		default:
		break;
	}
}

function logOut(){
	swal({
		title: "Logout?",
		text: "Anda yakin akan logout dari sesi ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Logout!",
		closeOnConfirm: true
	}, function () {
		window.localStorage.removeItem('mKasir_uid');
		document.location.reload();
	});
}

function cekLogin(){
	var uid = window.localStorage.getItem('mKasir_uid');
	if(uid){
		$("#loginPage").hide();
	}else{
		$("#loginPage").show().css('z-index','1031');
		$(".flogin").submit(function(e) {
			e.preventDefault();
            var username = $("#fUsername").val();
			var password = $("#fPassword").val();
			$("#loading, #ld").show();
			$.ajax({url:sUrl,
					type:"POST",
					timeout:10000,
					data:{p:'login',username:username,password:password},
					beforeSend: function(a){
						$("#loading, #ld").show();
					},
					success: function(data){
						$("#loading, #ld").hide();
						if(data.status=='error'){
							swal("Gagal", data.message, "error");
						}else if(data.status=='ok'){
							window.localStorage.setItem('mKasir_uid',data.ID);
							cekLogin();
							document.location.reload();
						}
					},error: function(a,b,c){
						$("#loading, #ld").hide();
						swal(a.statusText, "Terjadi kesalahan silahkan coba lagi! ", "error");
					},complete: function(a){
						$("#loading, #ld").hide();
					}
			});
        });
	}
	return uid;
}

function initApp(){
	cekTheme();
	var items_name = [];
    var items_qty = [];
    var items_jml = [];
	if(cekLogin()){
		autoSync();
		cekCart();
		getFr();
		loadPage();
		initComponent();
		autoHeight();
		cekNamaUsaha();
		$(window).resize(function(e) {
			var p = getUrlVars()[0];
			if(p=='file:' || p=='navbar' || p=='home'){
				$("#menu").show();
				$("#back").hide();
				$.AdminBSB.leftSideBar.activate();
			}else{
				$("#menu").hide();
				$("#back").show();
			}
			autoHeight();
		});

		$(document).on('click','.sprd',function(e){
			getPrd('');
		});

		$(document).on('click','.strx',function(e){
			getTrx('');
		});

		$(document).on('click', '.log-out', function(e){
            myApp.confirm('Anda yakin logout dari aplikasi?', function () {
                window.localStorage.removeItem('userLoggedIn');
                mainView.router.loadPage('pages/login.html');
                mainView.router.load('pages/login.html');
            });
        });

		$(document).on('click','.rcart',function(e){
					var cid = $(this).data('cid');
					var idx = $(this).data('idx');
					var cdata = '['+window.localStorage.getItem('mKasir_cartData').split("|").join(",")+']';
					var jitm = JSON.parse(cdata);
					var filtered = jitm.filter(function(item) {
					   return item.idx != idx;
					});
					var acd = JSON.stringify(filtered);
					var hcd = acd.split("},{").join("}|{").replace("[","").replace("]","");
					window.localStorage.setItem('mKasir_cartData',hcd);
					cekCart();
		});

		$(document).on('click','.item-prd',function(e){
							var i=0;
							if(i==0){
								var datap = $(this).data('produk');
								var stok = Number($(this).data('stok'));
								var dID = $(this).data('id');
								if(stok>0){
									var sk = Number(cekStok(dID));
									if(stok>=(sk+1)){
										addCart(datap);
									}else{
										swal("Stok Kurang!", "Stok produk ini tidak cukup!", "warning");
									}
								}else{
									swal("Kosong!", "Stok produk ini Kosong! ", "warning");
								}
								i++;
							}
						});

		$(document).on('click','.prtsl', function () {
		    $('.prd_trxl').each(function(index,item){ items_name[index] = $(this).text(); });
		    $('.qty_trxl').each(function(index,item){ items_qty[index] = $(this).text(); });
		    $('.jml_trxl').each(function(index,item){ items_jml[index] = $(this).text(); });
			var phtml = $('#shtlt').html();
			$("#printBodyl").html(phtml);
		});

		$(document).on('click',".prtstrukl",function(){
			var html = $("#shtlt").html();
			PrintElem(html, items_name, items_qty, items_jml);
		});

		$(document).on('click',"#logOut",function(){
			logOut();
		});
	}
}

function initHome(){
	$("#jumlah_prd").text(0);
	$("#count_trxs").text(0);
	$("#count_trxb").text(0);
	$("#count_ttl").text(0);
	$("#jumlah_trxs").text(0);
	$("#jumlah_trxb").text(0);
	$("#jumlah_ttl").text(0);
	$("#last_trx").text(0);
	$("#last_trx_tgl").text('-');
	var trx = window.localStorage.getItem('mKasir_trx');
	if(trx){
		var data = JSON.parse('{"trx":['+trx+']}');
		var count_trxs =0;
		var count_trxb =0;
		var jumlah_trxb=0;
		var jumlah_trxs=0;
		var count_ttl=0;
		var jumlah_ttl=0;
		var last_trx=0;
		var last_trx_tgl="-";
		for(var t=0;t<data.trx.length;t++){
			//console.log(data.trx[t]);
			if(data.trx[t].status=='Sukses'){
				count_trxs++;
				jumlah_trxs+=Number(data.trx[t].total);
			}else{
				count_trxb++;
				jumlah_trxb+=Number(data.trx[t].total);
			}
			count_ttl++;
			jumlah_ttl+=Number(data.trx[t].total);
		}
		last_trx=data.trx[0].total;
		last_trx_tgl=data.trx[0].waktu;
		$("#count_trxs").text(count_trxs);
		$("#count_trxb").text(count_trxb);
		$("#count_ttl").text(count_ttl);
		$("#jumlah_trxs").text(numberWithCommas(jumlah_trxs));
		$("#jumlah_trxb").text(numberWithCommas(jumlah_trxb));
		$("#jumlah_ttl").text(numberWithCommas(jumlah_ttl));
		$("#last_trx").text(numberWithCommas(last_trx)+' ('+data.trx[0].status+')');
		$("#last_trx_tgl").text(last_trx_tgl);
	}
	var prd = window.localStorage.getItem('mKasir_prd');
	if(prd){
		var data = JSON.parse(prd);
		var jumlah_prd=data.produk.length;
		$("#jumlah_prd").text(jumlah_prd);
	}
}

function autoSync(){
	var asp = Number(window.localStorage.getItem('mKasir_autoSyncPrd'));
	if(asp>0){
		$('[data-settings="auto-sync-produk"]').attr('checked',true);
		var i=0;
		getPrd(1);
		setInterval(function(){
			getPrd(1);
			console.log('produk '+i);
			i++;},asp);
	}else{
		$('[data-settings="auto-sync-produk"]').removeAttr('checked');
	}
	var ast = Number(window.localStorage.getItem('mKasir_autoSyncTrx'));
	if(ast>0){
		$('[data-settings="auto-sync-transaksi"]').attr('checked',true);
		var a =0;
		getTrx(1);
		setInterval(function(){
			getTrx(1);
			console.log('transaksi '+a);
			a++;
			},ast);
	}else{
		$('[data-settings="auto-sync-transaksi"]').removeAttr('checked');
	}
}

function cekNamaUsaha(){
	var n = window.localStorage.getItem('mKasir_NamaUsaha');
	if(n){
		$('[data-settings-val="nama-usaha"]').text(n);
		return n;
	}else{
		$('[data-settings-val="nama-usaha"]').text('mKasir');
		return 'mKasir';
	}
}

function cekTheme(){
	var theme = window.localStorage.getItem('mKasir_appTheme');
	if(theme){
		var a = $('*[class*="bg-"]:not(".badge,.notheme")');
		var b = a.removeClass(function (index, className) {
					return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
				});
		a.addClass('bg-'+theme);
		var c = $('body[class*="theme-"]');
		var d = c.removeClass(function (index, className) {
					return (className.match (/(^|\s)theme-\S+/g) || []).join(' ');
				});
		d.addClass('theme-'+theme);
		$('[data-settings="tema"] option[value="'+theme+'"').attr('selected','true');
	}
}

function cekAutoSyncPrd(){
	var asp = window.localStorage.getItem('mKasir_autoSyncPrd');
	if(asp=='true'){
		$('[data-settings="auto-sync-produk"]').attr('checked',true);
	}else{
		$('[data-settings="auto-sync-produk"]').removeAttr('checked');
	}
	autoSync();
	return asp;
}

function cekAutoSyncTrx(){
	var asp = window.localStorage.getItem('mKasir_autoSyncTrx');
	if(asp=='true'){
		$('[data-settings="auto-sync-transaksi"]').attr('checked',true);
	}else{
		$('[data-settings="auto-sync-transaksi"]').removeAttr('checked');
	}
	autoSync();
	return asp;
}

function filterList(elm,idList) {
    var filter, a, i;
    filter = $(elm).val().toUpperCase();
	var li = $(idList+' .list-group-item');
	a = li.find('.list-group-item-heading');
    for (i = 0; i < li.length; i++) {
        if (a.eq(i).text().toUpperCase().indexOf(filter) > -1) {
            li.eq(i).css('display','');
        } else {
            li.eq(i).css('display','none');
        }
	}
}

function Now(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	var hh = today.getHours();
	var ii = today.getMinutes();
	var ss = today.getSeconds();
	if(dd<10){dd = '0'+dd}
	if(mm<10) {mm = '0'+mm}
	if(hh<10){hh='0'+hh}
	if(ii<10){ii='0'+ii}
	if(ss<10){ss='0'+ss}
	return yyyy+'-'+mm+'-'+dd+' '+hh+':'+ii+':'+ss;
}

function addCart(datap){
		var cdata = window.localStorage.getItem('mKasir_cartData');
		if(cdata){
			var sp = cdata.split('|');
			var idx = sp.length;
			window.localStorage.setItem('mKasir_cartData','{"idx":"'+idx+'",'+datap+'}'+'|'+cdata);
		}else{
			window.localStorage.setItem('mKasir_cartData','{"idx":"0",'+datap+'}');
		}
		animDiv('#cart_con');
		try{
			cekCart();
		}catch(e){}
}

function cekStok(ID){
	var cdata = window.localStorage.getItem('mKasir_cartData');
	var cnt = 0;
	if(cdata){
		var dCart = JSON.parse('['+cdata.split('|').join(',')+']');
		for(var i=0;i<dCart.length;i++){
			if(Number(dCart[i].ID)==Number(ID)){
				cnt++;
			}
		}
	}
	return cnt;
}

function cekCart(){
	var cdata = window.localStorage.getItem('mKasir_cartData');
	if(cdata){
		var dCart = JSON.parse('['+cdata.split('|').join(',')+']');
		if(dCart.length<1){
			$('#cart_count').text('0');
			$('.cpdr').hide();
			$("#l_cartd").html('');
			$("#ttlcart").text('0');
			$("#byrbtn").hide();
		}else{
			$('#cart_count').text(dCart.length);
			var rcdata = cdata.split('|');
			var cc = '{"cart":[';
			for(var i=0;i<rcdata.length;i++){
				cc+=rcdata[i]+',';
			}
			cc=cc.substr(0,(cc.length)-1);
			cc+=']}';
			var cD = JSON.parse(cc);
			var cartD = $('#cartData').html();
			var compiledTemplate = Template7.compile(cartD);
			var context = cD;
			var html = compiledTemplate(context);
			$("#l_cartd").html(html);
			var total=0;
			for(var h=0;h<cD.cart.length;h++){
				total+=Math.ceil(Number(cD.cart[h].harga));
			}
			total = numberWithCommas(Math.ceil(total));
			$("#ttlcart").text(total);
			$("#byrbtn").show();
			try{
				dCart.sort();
				const count = dCart =>
				  dCart.reduce((a, b) =>
					Object.assign(a, {[b.ID]: (a[b.ID] || 0) + 1}), {});
				var cj = JSON.stringify(count(dCart));
				var lj = cj.replace('}','').replace('{','');
				var xj = lj.split(',');
				$(".cpdr").hide();
				for(var i=0;i<xj.length;i++){
					var xa = xj[i].split(':');
						if(xa[1]){
							//console.log(xa[0]);
							$('.cpdr[data-pid='+xa[0]+']').text(xa[1]);
							$('.cpdr[data-pid='+xa[0]+']').show();
						}
				}
			}catch(e){
				//console.log(e);
			}
		}
	}else{
		$('#cart_count').text('0');
		$('.cpdr').hide();
		$("#l_cartd").html('');
		$("#ttlcart").text('0');
		$("#byrbtn").hide();
	}
}

function numberWithCommas( x ) {
		return x.toString().replace( /\B(?=(\d{3})+(?!\d))/g, "," );
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

function upTrx(cData,id,waktu,subtotal,bayar,kembali,diskon,total,status){
	var cdt = cData.split('"').join("'");
	var data = '{"ID":"'+id+'","subtotal":"'+subtotal+'","bayar":"'+bayar+'","kembali":"'+kembali+'","diskon":"'+diskon+'","total":"'+total+'","ftotal":"'+numberWithCommas(total)+'","status":"'+status+'","waktu":"'+waktu+'","data":"'+cdt+'"}';
	var tr = window.localStorage.getItem('mKasir_trx');
	if(!tr){
		window.localStorage.setItem('mKasir_trx',data);
	}else{
		var newData = data+','+tr;
		window.localStorage.setItem('mKasir_trx',newData);
	}
}

function getTrx(auto){
	var trx = window.localStorage.getItem('mKasir_trx');
	if(!auto){
		$.ajax({url:sUrl,
			type:'POST',
			data:{p:'sync_trx',idata:trx},
			timeout:10000,
			async:'false',
			beforeSend: function(a){
				$(".page-loader-wrapper:not('#loginPage, #loading')").show();
			},error: function(a,b,c){
				swal(a.statusText, "Terjadi kesalahan silahkan coba lagi! ", "error");
				$(".page-loader-wrapper:not('#loginPage, #loading')").hide();
			},success: function(data){
				if(data.length>0){
					data = JSON.stringify(data);
					var ldata = data.substr(0,(data.length)-1);
					var rdata = ldata.substr(1);
					window.localStorage.setItem('mKasir_trx',rdata);
					initHome();
				}
				notif('Sikronisasi selesai');
				$(".page-loader-wrapper:not('#loginPage, #loading')").hide();
			},complete: function(a){
				$(".page-loader-wrapper:not('#loginPage, #loading')").hide();
			}
		});
	}else{
		$.post(sUrl,{p:'sync_trx',idata:trx},function(data){
			if(data.length>0){
				data = JSON.stringify(data);
				var ldata = data.substr(0,(data.length)-1);
				var rdata = ldata.substr(1);
				window.localStorage.setItem('mKasir_trx',rdata);
				initHome();
			}
		});
	}
}

function getTrxDetail(id){
	var data="";
	var tr = JSON.parse('['+window.localStorage.getItem('mKasir_trx')+']');
	for(var i=0;i<tr.length;i++){
		if(tr[i].ID==id){
			data = tr[i];
		}
	}
	return '['+JSON.stringify(data)+']';
}

function getPrd(auto){
	if(!auto){
		$.ajax({url:sUrl,
			type:'POST',
			data:{p:'get_produk'},
			timeout:10000,
			async:'false',
			beforeSend: function(a){
				$(".page-loader-wrapper:not('#loginPage, #loading')").show();
			},error: function(a,b,c){
				swal(a.statusText, "Terjadi kesalahan silahkan coba lagi!", "error");
				$(".page-loader-wrapper:not('#loginPage, #loading')").hide();
			},success: function(data){
				if(data.produk.length>0){
					window.localStorage.setItem('mKasir_prd',JSON.stringify(data));
					initHome();
				}
				notif('Sikronisasi selesai');
				$(".page-loader-wrapper:not('#loginPage, #loading')").hide();
			},complete: function(a){
				$(".page-loader-wrapper:not('#loginPage, #loading')").hide();
			}
		});
	}else{
		$.post(sUrl,{p:'get_produk'},function(data){
			if(data.produk.length>0){
				window.localStorage.setItem('mKasir_prd',JSON.stringify(data));
			}
			initHome();
		});
	}
}

function getPrdDetail(id){
	var data="";
	var prd = JSON.parse(window.localStorage.getItem('mKasir_prd'));
	for(var i=0;i<prd.produk.length;i++){
		if(prd.produk[i].ID==id){
			data = prd.produk[i];
		}
	}
	return '{"produk":['+JSON.stringify(data)+']}';
}

function updatePrd(id,key,val){
	var fdata="";
	var ndata="";
	var prd = JSON.parse(window.localStorage.getItem('mKasir_prd'));
	//console.log(JSON.stringify(prd));
	for(var i=0;i<prd.produk.length;i++){
		if(prd.produk[i].ID==id){
			udata = prd.produk[i][key]=val;
			fdata = prd.produk[i];
		}else{
			ndata+= JSON.stringify(prd.produk[i])+',';
		}
	}
	rdata = ndata.substr(0,(ndata.length-1));
	var newData = '{"produk":['+JSON.stringify(fdata)+','+rdata+']}';
	window.localStorage.setItem('mKasir_prd',newData);
}

function cekSupport(){
	if(!window.localStorage || typeof(Storage)=='undefined'){
		swal({
			title: "Perangkat tidak didukung",
			text: "Sayangnya perangkat anda belum mendukung aplikasi mKasir :(",
			type: "warning"
			}, function(){
				try{
					  Android.keluarApp();
				  }catch(error){
				  }
			});
		$("nav, section").hide();
		return false;
	}else{
		return true;
	}
}

function PrintElem(elemt, items_name, items_qty, items_jml){

    if(elemt == "phtml"){
        var HeadStr = [" (BILL)"];
        HeadStr.push($('#id_trx').text());
        HeadStr.push($('#tgl_trx').text());
        HeadStr.push($('#trx_btotal').text());
        HeadStr.push($('#trx_bdiskon').text());
        HeadStr.push($('#trx_gtotal').text());
        HeadStr.push($('#trx_jby').text());
        HeadStr.push($('#trx_kmb').text());
    }else{
        var HeadStr = [" (History Transaction)"];
        HeadStr.push($('#id_trxl').text());
        HeadStr.push($('#tgl_trxl').text());
        HeadStr.push($('#trxl_btotal').text());
        HeadStr.push($('#trxl_bdiskon').text());
        HeadStr.push($('#trxl_gtotal').text());
        HeadStr.push($('#trxl_jby').text());
        HeadStr.push($('#trxl_kmb').text());
    }

    try{
        Android.loadExternal(HeadStr, items_name, items_qty, items_jml);
    }catch(e){
        //console.log(e);
    }
}
