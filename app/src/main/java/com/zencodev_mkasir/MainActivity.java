package com.zencodev_mkasir;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.zencodev_mkasir.util.GPSTracker;
import com.zencodev_mkasir.util.IabBroadcastReceiver;
import com.zencodev_mkasir.util.NetworkHandler;
import com.zencodev_mkasir.util.PermissionUtil;
import com.zencodev_mkasir.util.UrlHander;
import com.zencodev_mkasir.util.WebAppInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,DownloadListener, IabBroadcastReceiver.IabBroadcastListener {

    private static final int FILE_CHOOSER_RESULT_CODE = 1;
    private static final int REQUEST_SELECT_FILE = 2;
    /* URL saved to be loaded after fb login */
    private static String target_url, target_url_prefix;
    public ValueCallback<Uri[]> uploadMessage;
    String nurl;
    //PAYMENT
    // Provides purchase notification while this app is running
    IabBroadcastReceiver mBroadcastReceiver;
    //DATA FOR GEOLOCAION REQUEST
    String geoLocationOrigin;
    GeolocationPermissions.Callback geoLocationCallback;
    String _latitude, _longitude;
    Location mLastLocation;
    private Context mContext;
    private WebView mWebview, mWebviewPop;
    private Button btnClick;
    private ValueCallback<Uri> mUploadMessage;
    private FrameLayout mContainer;
    private String urlData, currentUrl, contentDisposition, mimeType;
    private GPSTracker gpsTracker = new GPSTracker(this);
    String Print;
    File dir;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkURL(getIntent());
        initComponents();
        initBrowser(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("html")) {
                mWebview.loadData(extras.getString("html"),"text/html","utf-8");
                Print = "OK";
            }
        }
        showContent();
    }


    private void checkURL(Intent intent) {
        if (intent != null) {
            if ("text/plain".equals(intent.getType()) && !TextUtils.isEmpty(intent.getStringExtra(Intent.EXTRA_TEXT))) {
                target_url = intent.getStringExtra(Intent.EXTRA_TEXT);
                target_url_prefix = Uri.parse(target_url).getHost();
                currentUrl = target_url;
                return;
            }
        }

        target_url = "file:///android_asset/index.html";

        if (TextUtils.isEmpty(target_url)) {
            target_url = "file:///android_asset/index.html";
            target_url_prefix = "android_asset";
        } else {
            target_url_prefix = Uri.parse(target_url).getHost();
        }

        currentUrl = target_url;

        if (mWebview != null) {
            if (mWebviewPop != null) {
                mWebviewPop.setVisibility(View.GONE);
                mContainer.removeView(mWebviewPop);
                mWebviewPop = null;
            }
            mWebview.setVisibility(View.VISIBLE);
        }
    }

    public  void keluarApp(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getApplicationContext());
        builder.setMessage("Apakah Anda Benar-Benar ingin keluar?")
                .setCancelable(false)
                .setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                finish();
                                System.exit(0);
                            }
                        })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkURL(getIntent());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebview.saveState(outState);
    }


    private void initComponents() {
        mContext = this.getApplicationContext();
    }

    public void showContent() {

        mContainer.setVisibility(View.VISIBLE);
        currentUrl = mWebview.getUrl();
        //ProgressDialogHelper.showProgress(MainActivity.this);
    }


    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    private void initBrowser(Bundle savedInstanceState) {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        mWebview = (WebView) findViewById(R.id.webview);
        mContainer = (FrameLayout) findViewById(R.id.webview_frame);
        btnClick = (Button) findViewById(R.id.btnclick);
        File dir = getCacheDir();
        if (!dir.exists()) {
            dir.mkdirs();
        }

        WebSettings webSettings = mWebview.getSettings();
        webSettings.setAppCacheMaxSize(1024 * 1024 * 8);
        webSettings.setAppCachePath(dir.getPath());
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setGeolocationEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        String databasePath = getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setDatabasePath(databasePath);
        webSettings.setGeolocationEnabled(true);
        webSettings.setGeolocationDatabasePath(getFilesDir().getPath());
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setAllowFileAccess(true);
        if (!NetworkHandler.isNetworkAvailable(mWebview.getContext())) {
            webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        } else {
            webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        }
        mWebview.addJavascriptInterface(new WebAppInterface(MainActivity.this, mWebview, btnClick), "Android");
        mWebview.setWebViewClient(new UriWebViewClient());
        mWebview.setWebChromeClient(new UriChromeClient());
        if (Build.VERSION.SDK_INT >= 19) {
            mWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= 15 && Build.VERSION.SDK_INT < 19) {
            mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        if (savedInstanceState != null) {
            mWebview.restoreState(savedInstanceState);
        } else {
            mWebview.loadUrl(target_url);
        }
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(mContext, "Test click", Toast.LENGTH_SHORT).show();
        switch (view.getId()) {

        }
    }

    @Override
    public void receivedBroadcast() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWebview != null) {
            mWebview.destroy();
        }
        if (mWebviewPop != null) {
            mWebviewPop.destroy();
        }

        if (mBroadcastReceiver != null) {
            unregisterReceiver(mBroadcastReceiver);
        }
    }

    @Override
    public void onBackPressed() {
            if (mWebview.canGoBack()) {
                mWebview.goBack();
            } else {
                keluarApp();
                super.onBackPressed();
            }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILE_CHOOSER_RESULT_CODE || requestCode == REQUEST_SELECT_FILE ) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (requestCode == REQUEST_SELECT_FILE) {
                    if (uploadMessage == null)
                        return;

                    Uri uri[] = null;
                    if (data != null) {
                        if (data.getClipData() != null) {
                            uri = new Uri[data.getClipData().getItemCount()];
                            for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                                uri[i] = data.getClipData().getItemAt(i).getUri();
                            }
                        }  else {
                            uri = WebChromeClient.FileChooserParams.parseResult(resultCode, data);
                        }
                    }

                    uploadMessage.onReceiveValue(uri);
                    uploadMessage = null;
                }
            } else if (requestCode == FILE_CHOOSER_RESULT_CODE) {
                if (null == mUploadMessage) return;
                // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
                // Use RESULT_OK only if you're implementing WebView inside an Activity
                Uri result = data == null || resultCode != MainActivity.RESULT_OK ? null : data.getData();
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            } else {
                Toast.makeText(MainActivity.this.getApplicationContext(), R.string.failed_to_upload_image, Toast.LENGTH_LONG).show();
            }
        } else {
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long l) {
        this.contentDisposition = contentDisposition;
        this.mimeType = mimeType;
        UrlHander.downladLink(this, url, contentDisposition, mimeType);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == PermissionUtil.MY_PERMISSIONS_REQUEST_CALL) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //UrlHander.call(MainActivity.this, urlData);
            }
        } else if (requestCode == PermissionUtil.MY_PERMISSIONS_REQUEST_SMS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //UrlHander.sms(MainActivity.this, urlData);
            }
        } else if (requestCode == PermissionUtil.MY_PERMISSIONS_REQUEST_DOWNLOAD) {
            UrlHander.download(MainActivity.this, urlData, contentDisposition, mimeType);
        } else if (requestCode == PermissionUtil.MY_PERMISSIONS_REQUEST_GEOLOCATION) {
            if (geoLocationCallback != null) {
                geoLocationCallback.invoke(geoLocationOrigin, true, false);
            }
        }
    }

    public List<Intent> getCameraIntents() {
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent i = new Intent(captureIntent);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            i.setPackage(packageName);
            cameraIntents.add(i);
        }
        return cameraIntents;
    }

    private class UriWebViewClient extends WebViewClient {
        private int error;

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
            if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
                mWebview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                if (error != 1) {
                    //Toast.makeText(mContext, "Koneksi Error! Offline Mode : ON", Toast.LENGTH_SHORT).show();
                    error = 1;
                }
            } else {
                mWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                if (error != 2) {
                   // Toast.makeText(mContext, "Koneksi Sukses! Offline Mode : OFF", Toast.LENGTH_SHORT).show();
                    error = 2;
                }
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            // On load Spinner visible
            //ProgressDialogHelper.showProgress(MainActivity.this);
        }



        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            urlData = url;
            if (target_url_prefix.equals(host)) {
                if (mWebviewPop != null) {
                    mWebviewPop.setVisibility(View.GONE);
                    mContainer.removeView(mWebviewPop);
                    mWebviewPop = null;
                }
                return false;
            }

            boolean result = UrlHander.checkUrl(MainActivity.this, url);
            currentUrl = url;
            //ProgressDialogHelper.showProgress(MainActivity.this);
            return result;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
                view.loadUrl("file:///android_asset/NoInternet.html");
            }
            //ProgressDialogHelper.dismissProgress();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
                view.loadUrl("file:///android_asset/NoInternet.html");
            }
            //ProgressDialogHelper.dismissProgress();
        }

        //@Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            //super.onReceivedHttpError(view, request, errorResponse);
            if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
                view.loadUrl("file:///android_asset/NoInternet.html");
            }
            //ProgressDialogHelper.dismissProgress();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            showContent();
            //ProgressDialogHelper.dismissProgress();
            //view.clearCache(true);
        }


    }

    class UriChromeClient extends WebChromeClient {

        @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
        @Override

        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            File dir = getCacheDir();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            mWebviewPop = new WebView(mContext);
            mWebviewPop.setVerticalScrollBarEnabled(false);
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.setWebViewClient(new UriWebViewClient());
            mWebviewPop.getSettings().setAppCacheMaxSize(1024 * 1024 * 8);
            mWebviewPop.getSettings().setAppCachePath(dir.getPath());
            mWebviewPop.getSettings().setJavaScriptEnabled(true);
            mWebviewPop.getSettings().setSavePassword(false);
            mWebviewPop.getSettings().setAppCacheEnabled(true);
            mWebviewPop.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mWebviewPop.getSettings().setSupportMultipleWindows(true);
            mWebviewPop.getSettings().setGeolocationEnabled(true);
            mWebviewPop.getSettings().setDomStorageEnabled(true);
            mWebviewPop.getSettings().setDatabaseEnabled(true);
            String databasePath = getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
            mWebviewPop.getSettings().setDatabasePath(databasePath);
            mWebviewPop.getSettings().setGeolocationEnabled(true);
            mWebviewPop.getSettings().setGeolocationDatabasePath(getFilesDir().getPath());
            mWebviewPop.getSettings().setLoadWithOverviewMode(true);
            mWebviewPop.getSettings().setAllowFileAccess(true);
            mWebviewPop.addJavascriptInterface(new WebAppInterface(MainActivity.this, mWebviewPop, btnClick), "Android");
            if (!NetworkHandler.isNetworkAvailable(mWebviewPop.getContext())) {
                mWebviewPop.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            } else {
                mWebviewPop.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            }
            // mWebviewPop.getSettings().setUserAgentString("Treigners_App");
            mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            mContainer.addView(mWebviewPop);

            if (Build.VERSION.SDK_INT >= 19) {
                mWebviewPop.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else if (Build.VERSION.SDK_INT >= 15 && Build.VERSION.SDK_INT < 19) {
                mWebviewPop.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();
            return true;
        }


        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.v("TEST", "onCloseWindow");
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(final String origin,
                                                       final GeolocationPermissions.Callback callback) {
            // Always grant permission since the app itself requires location
            // permission and the user has therefore already granted it
            MainActivity.this.geoLocationOrigin = origin;
            MainActivity.this.geoLocationCallback = callback;
        }
        // openFileChooser for Android 3.0+
        protected void openFileChooser(ValueCallback uploadMsg, String acceptType) {
            mUploadMessage = uploadMsg;
            List<Intent> cameraIntents = getCameraIntents();
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setType("image/*");
            try {
                Intent chooserIntent = Intent.createChooser(intent, getString(R.string.file_browser));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(chooserIntent, FILE_CHOOSER_RESULT_CODE);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(MainActivity.this.getApplicationContext(),
                        R.string.cannot_open_file_chooser,
                        Toast.LENGTH_LONG).show();
            }
        }

        // For Lollipop 5.0+ Devices
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback,
                                         WebChromeClient.FileChooserParams fileChooserParams)
        {
            if (mUploadMessage != null) {
                uploadMessage.onReceiveValue(null);
                uploadMessage = null;
            }

            uploadMessage = filePathCallback;

            List<Intent> cameraIntents = getCameraIntents();
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");

            try {
                Intent chooserIntent = Intent.createChooser(intent, getString(R.string.file_browser));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(chooserIntent, REQUEST_SELECT_FILE);
            } catch (ActivityNotFoundException e) {
                uploadMessage = null;
                Toast.makeText(MainActivity.this.getApplicationContext(),
                        R.string.cannot_open_file_chooser,
                        Toast.LENGTH_LONG).show();
                return false;
            }
            return true;
        }

        // openFileChooser for Android < 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            mUploadMessage = uploadMsg;
            List<Intent> cameraIntents = getCameraIntents();
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setType("image/*");
            try {
                Intent chooserIntent = Intent.createChooser(intent, getString(R.string.file_browser));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(chooserIntent, FILE_CHOOSER_RESULT_CODE);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(MainActivity.this.getApplicationContext(),
                        R.string.cannot_open_file_chooser,
                        Toast.LENGTH_LONG).show();
            }
        }

        //For Android 4.1 only
        protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)
        {
            mUploadMessage = uploadMsg;
            List<Intent> cameraIntents = getCameraIntents();
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setType("image/*");
            try {
                Intent chooserIntent = Intent.createChooser(intent, getString(R.string.file_browser));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(chooserIntent, FILE_CHOOSER_RESULT_CODE);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(MainActivity.this.getApplicationContext(),
                        R.string.cannot_open_file_chooser,
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}

