package com.zencodev_mkasir.util;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

import com.zencodev_mkasir.MainActivity;

/**
 * Created by dragank on 2/20/2017.
 */

public class WebAppInterface  {
    private Context mContext;
    private String productId;
    private WebView mWebview;
    private Button btnClick;
    MainActivity mainActivity = new MainActivity();
    /** Instantiate the interface and set the context */
    public WebAppInterface(Context mContext, WebView mWebview, Button btnClick) {
        this.mContext = mContext;
        this.mWebview = mWebview;
        this.btnClick = btnClick;
    }

    @JavascriptInterface
    public void createNotification(String displayName, String message, String ID) {
        if(message.length()>0 || displayName.length()>0) {
            LocalNotification.createNotification(mContext, displayName, message, ID);
        }
    }

    @JavascriptInterface
    public void showLoader() {
        ProgressDialogHelper.showProgress(mContext);
    }

    @JavascriptInterface
    public void hideLoader() {
        ProgressDialogHelper.dismissProgress();
    }

    @JavascriptInterface
    public void fontSizeNormal() {
        mWebview.post(new Runnable() {
            @Override
            public void run() {
                WebSettings webSettings = mWebview.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.NORMAL);
            }
        });
    }

    @JavascriptInterface
    public void fontSizeLarger() {
        mWebview.post(new Runnable() {
            @Override
            public void run() {
                WebSettings webSettings = mWebview.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.LARGER);
            }
        });
    }

    @JavascriptInterface
    public void fontSizeLargest() {
        mWebview.post(new Runnable() {
            @Override
            public void run() {
                WebSettings webSettings = mWebview.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.LARGEST);
            }
        });
    }

    @JavascriptInterface
    public void fontSizeSmaller() {
        mWebview.post(new Runnable() {
            @Override
            public void run() {
                WebSettings webSettings = mWebview.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.SMALLER);
            }
        });

    }

    @JavascriptInterface
    public void fontSizeSmallest() {
        mWebview.post(new Runnable() {
            @Override
            public void run() {
                WebSettings webSettings = mWebview.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.SMALLEST);
            }
        });
    }

    @JavascriptInterface
    public void playSound() throws InterruptedException {
        /*Integer i = 0;
        MediaPlayer mp = MediaPlayer.create(mContext, R.raw.click);
        mp.setVolume(5,5);
        if(i==0) {
            mp.start();
            wait(500);
            mp.stop();
            i=1;
        }*/
    }

    @JavascriptInterface
    public void stopSound(){
        //mp.stop();
    }

    @JavascriptInterface
    public void keluarApp(){
        mainActivity.finish();
        System.exit(0);
    }

    @JavascriptInterface
    public void loadExternal(String[] header, String[] items_name, String[] items_qty, String[] items_jml){
        String[][] str1 = new String[items_name.length][3];
        for(int i=0; i < items_name.length; i++){
            str1[i][0] = items_name[i];
            str1[i][1] = items_qty[i];
            str1[i][2] = items_jml[i];
        }
        LocalNotification.PrintHtml(mContext,mWebview,header,str1);

//        Toast.makeText(mContext, str1[0][0]+"|"+str1[0][1]+"|"+str1[0][2], Toast.LENGTH_LONG).show();
    }

    //@JavascriptInterface
    //public void createWebPrintJob(String html) {
       // mainActivity.PrintHtml(html);
    //}

    @JavascriptInterface
    public String cekUrl(String url){
        return url;
    }
}

