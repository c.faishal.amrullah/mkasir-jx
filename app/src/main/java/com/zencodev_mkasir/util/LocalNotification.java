package com.zencodev_mkasir.util;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.zencodev_mkasir.MainActivity;
import com.zencodev_mkasir.R;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.UUID;


/**
 * Created by DRAGAN on 3/5/2017.
 */

public class LocalNotification {
    //JX print BILL - [23-01-2018][ADD VARIABLE]--------------//
    private static final UUID SPP_UUID = //UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static BluetoothAdapter mBluetoothAdapter = null;
    private static BluetoothDevice mmDevice;
    private static byte FONT_TYPE;
    private static BluetoothSocket btsocket;
    private static OutputStream btoutputstream;
    private static String BILL;
    private static int numOfCharInOneLine = 32;
    //END**JX print BILL - [23-01-2018][ADD VARIABLE]---------//

    public static void createNotification(Context context, String displayName, String message, String ID) {

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra("url", "file:///android_asset/index.html#notif/baca=" + ID);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.startActivities();
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager nm =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(displayName)
                        .setContentText(message);

        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setContentIntent(resultPendingIntent);
        nm.notify(0, builder.build());
    }

    public static void saveStringToHtmlFile(Context context, String htmlString) {
        File htmlFile = null;
        try {
            // If the file does not exists, it is created.
            htmlFile = new File(context.getExternalFilesDir(null), "print.html");
            if (!htmlFile.exists()) {
                htmlFile.createNewFile();
            }
            // Adds a line to the file
            BufferedWriter writer = new BufferedWriter(new FileWriter(htmlFile, false));
            writer.write(htmlString);
            writer.close();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(htmlFile), "text/html");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (IOException e) {
            String a = e.toString();
            Toast.makeText(context, "Gagal : " + a, Toast.LENGTH_LONG).show();
            //Log.e(Tag, "Unable to write to HTML_FILE_NAME.html file.");
        }
    }

    public static void PrintHtml(Context context, final WebView webView, final String[] header, final String[][] items) {
        // webView.post(new Runnable() {
        //     @Override
        //     public void run() {
        //         webView.loadData(html, "text/html", "utf-8");
        //     }
        // });
        //ScreenShoot(context);
        if(BillCreate(header, items)){
            Toast.makeText(context,PrintBt(),Toast.LENGTH_LONG).show();
        }

        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        //     PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
        //     PrintDocumentAdapter printDocumentAdapter = webView.createPrintDocumentAdapter();
        //     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        //         printDocumentAdapter = webView.createPrintDocumentAdapter("Struk mKasir");
        //     String documentName = "mKasir Struk";
        //     PrintAttributes.Builder builder = new PrintAttributes.Builder();
        //     builder.setMediaSize(PrintAttributes.MediaSize.ISO_A6);
        //     PrintJob printJob = printManager.print(documentName, printDocumentAdapter, builder.build());
        //     if (printJob.isCompleted()) {
        //         Toast.makeText(context, "Print Selsai", Toast.LENGTH_LONG).show();
        //     } else if (printJob.isFailed()) {
        //         Toast.makeText(context, "Print Gagal", Toast.LENGTH_LONG).show();
        //     }
        // } else {
        //     Toast.makeText(context, "Print tidak didukung", Toast.LENGTH_LONG).show();
        // }
    }

    public static boolean BillCreate(String[] header, String[][] items){
        BILL  = "                                ";
        BILL += "                                ";
        BILL += "mKasir" + header[0] + generateSpace(header[0].length() + 6);
        BILL += "                                ";
        BILL += "ID  : " + header[1] + generateSpace(header[1].length() + 6);
        BILL += "Tgl : " + header[2] + generateSpace(header[2].length() + 6);
        BILL += "--------------------------------";
        for(int i =0; i < items.length; i++){
            BILL += billTextLeftRight(items[i][0] + " (QTY) : " + items[i][1], items[i][2], 32);
        }
        BILL += "--------------------------------";
        BILL += "                                ";
        BILL += "Subtotal    : " + generateSpace(header[3].length()+14) + header[3];
        BILL += "Diskon      : " + generateSpace(header[4].length()+14) + header[4];
        BILL += "Grand Total : " + generateSpace(header[5].length()+14) + header[5];
        BILL += "Tunai       : " + generateSpace(header[6].length()+14) + header[6];
        BILL += "Kembali     : " + generateSpace(header[7].length()+14) + header[7];
        BILL += "                                ";
        BILL += "                                ";
        BILL += " Barang yang sudah dibeli tidak ";
        BILL += "       dapat dikembalikan.      ";
        BILL += "      ----Terima Kasih----      ";
        BILL += "                                ";
        BILL += "                                ";
        BILL += "                                ";
        return true;
    }

    public static void ScreenShoot(Context context) {
        View rootView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Bitmap screen;
        View v1 = rootView.getRootView();
        v1.setDrawingCacheEnabled(true);
        screen = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        String date = SimpleDateFormat.getDateInstance().format(new Date());
        String filename = "Struk-"+date.toString()+".bmp";
        File file = new File(dirPath, filename);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            screen.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(dirPath, filename));
            intent.setDataAndType(uri, "image/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String PrintBt(){
        String returnStatus;

        try{
            if(btsocket != null){
                destroy();
            }

            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if(pairedDevices.size() > 0) {
                //get first connected bluetooth
                int i = 0;
                for (BluetoothDevice device : pairedDevices) {
                    if (i == 0) { mmDevice = device; }
                    i++;
                }
            }

            try { btsocket = mmDevice.createRfcommSocketToServiceRecord(SPP_UUID); } catch (IOException e) {}
            mBluetoothAdapter.cancelDiscovery();
            try {
                btsocket.connect();
            } catch (IOException e) {
                try {
                    btsocket.close();
                } catch (IOException e1) {}
            }

            try { btoutputstream = btsocket.getOutputStream(); } catch (IOException e) {}

            byte[] printformat = {0x1B, 0x21, FONT_TYPE};
            btoutputstream.write(printformat);
            btoutputstream.write(BILL.getBytes());
            btoutputstream.write(0x0D);
            btoutputstream.write(0x0D);
            returnStatus = BILL;

        }catch (Exception e){
            returnStatus = "print failed";
        }

        return returnStatus;
    }

    public static String generateSpace(int Char) {
        String space = "";
        for (int i = 0; i < numOfCharInOneLine - Char; i++) {
            space += " ";
        }

        return space;
    }

    public static String billTextLeftRight(String left, String right, int maxLine){
        String[] stringArray;
        String tmpString = "";
        String finalString = "";
        String count = "";

        if(left.length()+right.length() > maxLine){
            stringArray = left.split(" (QTY) ");
            left = stringArray[0];
            count = stringArray[1];
            maxLine = maxLine-right.length();

            if(left.length() > maxLine){
                stringArray = left.split(" ");

                for (String singleWord : stringArray){

                    if((tmpString + singleWord + " ").length() > maxLine){
                        tmpString = tmpString.substring(0, tmpString.length()-1);

                        if(finalString.equals("")){
                            tmpString = tmpString +generateSpace(tmpString.length()+right.length())+ right;
                        }

                        finalString += tmpString;
                        tmpString = singleWord + " ";
                    }else{
                        tmpString += singleWord + " ";
                    }
                }
                finalString += tmpString + "(QTY) " + count + "\n";
            }else{
                finalString = left + generateSpace(left.length()+right.length()) + right;
                finalString += "(QTY) " + stringArray[1] + "\n";
            }

        }else{
            finalString = left + generateSpace(left.length()+right.length()) + right;
        }
        return finalString;
    }

    public static void destroy(){
        try {
            if (btsocket != null) {
                btoutputstream.close();
                btsocket.close();
                btsocket = null;
            }
        } catch (IOException e) {}
    }
}
