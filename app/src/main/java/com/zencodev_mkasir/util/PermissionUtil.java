package com.zencodev_mkasir.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.webkit.GeolocationPermissions;


public class PermissionUtil {

    public static final int MY_PERMISSIONS_REQUEST_CALL = 10;
    public static final int MY_PERMISSIONS_REQUEST_SMS = 11;
    public static final int MY_PERMISSIONS_REQUEST_DOWNLOAD = 12;
    public static final int MY_PERMISSIONS_REQUEST_GEOLOCATION= 13;

    //We are calling this method to check the permission status
    public static boolean isPermissionAllowed(Context mContext, String permission) {
        //Getting the permission status
        int result = PackageManager.PERMISSION_GRANTED;

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    public static void requestPermission(Activity mActivity, String[] permision, int permisionNumer) {

        //And finally ask for the permission
        requestPermission(mActivity, new String[]{String.valueOf(permision)}, permisionNumer);
    }

    public static void geoLocationPermission(Activity mActivity, String origin, GeolocationPermissions.Callback callback) {
        if (PermissionUtil.isPermissionAllowed(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            callback.invoke(origin, true, false);
        }
        PermissionUtil.requestPermission(mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PermissionUtil.MY_PERMISSIONS_REQUEST_GEOLOCATION);
    }

    public static void checkPermissions(Activity mActivity, String[] permissions) {
        boolean needsPermission = false;
        for (String permission : permissions) {
            int permissionCheck = PackageManager.PERMISSION_GRANTED;
            if (PackageManager.PERMISSION_GRANTED != permissionCheck) {
                needsPermission = true;
                break;
            }
        }

        if (needsPermission) {
            requestPermission(mActivity, permissions, 0);
        }
    }
}
